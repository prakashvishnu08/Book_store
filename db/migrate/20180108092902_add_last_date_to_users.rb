class AddLastDateToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_date, :string
    add_column :users, :date, :string
  end
end
