class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :plan, foreign_key: true
    add_column :users, :name, :string
    add_column :users, :phone, :string
  end
end
