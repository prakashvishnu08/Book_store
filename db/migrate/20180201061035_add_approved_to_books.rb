class AddApprovedToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :approved, :boolean, default: false
  end
end
