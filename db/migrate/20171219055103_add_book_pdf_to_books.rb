class AddBookPdfToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :book_pdf, :string
  end
end
