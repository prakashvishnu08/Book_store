class PlansController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]
  def index
    @plans = Plan.all
  end

  def show
    @plan = Plan.find(params[:id])
  end

end
