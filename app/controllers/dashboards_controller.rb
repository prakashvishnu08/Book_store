class DashboardsController < ApplicationController
	skip_before_action :authenticate_user!

  def index
    @books = Book.limit(10)
  end

end
