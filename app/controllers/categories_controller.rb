class CategoriesController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:index]

  def index
    @categories = Category.all
    if params[:id].present?
      @category = Category.find(params[:id])
      @books = @category.books.page(params[:page]).per(8)
    else
      @category = Category.first
      @books = @category.books.page(params[:page]).per(8)
    end
  end

  def category_books
    @category = Category.find(params[:id])
    @books = @category.books.where(approved: true)
  end

end
