class BooksController < ApplicationController
  load_and_authorize_resource
  skip_authorize_resource only: [:author_books, :authors]
  skip_before_filter :authenticate_user!, only: [:index, :authors, :author_books]

  def index
    @q = Book.ransack(params[:q])
    @books = @q.result(distinct: true).includes(:author).page(params[:page]).per(8)
  end

  def new
    @book = Book.new
    authorize! :new, @book
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      # admin = User.find_by(role_id: 3)
      # BookStoreMailer.new_book(@book, admin).deliver
      flash[:success] = "Book Successfully Created"
      redirect_to books_path
    else
      render "new"
    end
  end

  def show
    @book = Book.find(params[:id])
  end

  def edit
    @book = Book.find(params[:id])
  end

  def authors
    @authors = User.where(role_id: 1)
  end

  def author_books
    @authors = User.select{|x| x.books.present?}
    if params[:user_id].present?
      @author = User.find(params[:user_id])
      @books = @author.books.page(params[:page]).per(8)
    else
      @author = @authors.first
      @books = @author.books.page(params[:page]).per(8)
    end
  end

  def open_book
    @book = Book.find(params[:id])
    redirect_to pdfjs.minimal_path(file: @book.book_pdf.url)
  end

  def update
    @book = Book.find(params[:id])
    if @book.update(book_params)
      flash[:success] = "Book updated"
      redirect_to book_path(@book)
    else
      render "edit"
    end
  end

  def verify_books
    @books = Book.where(approved: false).order(:created_at)
  end

  def approve_book
    @book = Book.find(params[:id])
    @book.approved = true
    @book.save
    flash[:success] = "Book Successfully verified"
    redirect_to verify_books_books_path
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    flash[:danger] = "Book deleted"
    if current_user.admin?
      redirect_to verify_books_books_path
    else
      redirect_to books_path
    end
  end

  def my_books
    @books = current_user.books.page(params[:page]).per(8)
  end

  private

  def book_params
    params.require(:book).permit(:name, :book_pdf, :category_id, :description, :avatar).merge(user_id: current_user.id)
  end

end
