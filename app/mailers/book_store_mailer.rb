class BookStoreMailer < ApplicationMailer
  default from: 'demo@bookstore.com'
  layout 'mailer'

  def new_book(book, admin)
    @book = book
    @admin = admin
    mail(to: @admin.email, subject: 'New Book Notification')
  end

end
