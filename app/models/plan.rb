class Plan < ApplicationRecord
  has_many :users

  def trial?
    name == "Trial Plan"
  end
end
