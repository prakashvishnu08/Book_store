class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
         :omniauth_providers => [:google_oauth2]
  belongs_to :role, required: true
  belongs_to :plan, required: false
  has_many :books
  accepts_nested_attributes_for :role
  validates :name, length: {maximum: 15, minimum: 6}, presence: true
  validates :phone, length: {maximum: 15}, presence: true
  validates_confirmation_of :password_confirmation, :on => :create
  before_save :default_plan

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
    end
  end

  def author?
    role.name == "Author"
  end

  def reader?
    role.name == "Reader"
  end

  def admin?
    role.name == "admin"
  end

  def user_valid?
    last_date.to_date >= Date.today
  end

  private

    def default_plan
      self.plan_id ||= Plan.first.id
      self.last_date ||= Date.today+30.days
    end

end
