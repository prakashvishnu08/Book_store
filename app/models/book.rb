class Book < ApplicationRecord
  require 'carrierwave/orm/activerecord'
  belongs_to :author, foreign_key: "user_id", class_name: "User"
  belongs_to :category, required: true
  validates :name, :user_id, :category_id, :description, :book_pdf, :avatar, presence: :true
  mount_uploader :book_pdf, BookPdfUploader
  mount_uploader :avatar, AvatarUploader
end
