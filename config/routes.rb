Rails.application.routes.draw do

  get 'charges/new'

  get 'charges/create'

  mount PdfjsViewer::Rails::Engine => "/pdfjs", as: 'pdfjs'
  devise_for :users, :controllers => {
    :omniauth_callbacks => "users/omniauth_callbacks"
  }
  root to: 'dashboards#index'
  resources :books, only: [:index, :new, :create, :show, :edit, :update, :destroy] do
    collection do
      get "authors"
      get "author_books"
      get "verify_books"
      get "my_books"
    end
    member do
      get "approve_book"
    end
  end
  resources :categories, only: [:index] do
    collection do
      get 'category_books'
    end
  end
  resources :plans, only: [:index, :show]
  resources :charges, only: [:new, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
